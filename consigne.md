

Si vous avez commencé ce projet avant le 07/12/2022, vous avez débuté votre travail sur ce projet archivé. Vous pouvez continuer l'existant ou recommencer avec ce nouveau projet, dont les modifications s’alignent avec les retours étudiants.

Vous êtes ingénieur IA chez MIC (Marketing Intelligence Consulting), une entreprise de conseil spécialisée sur les problématiques de marketing digital.

Dans deux semaines, vous avez rendez-vous avec Mme Aline, directrice marketing de la compagnie aérienne “Air Paradis”.


Logo Air Paradis


Air Paradis a missionné votre cabinet pour créer un produit IA permettant d’anticiper les bad buzz sur les réseaux sociaux. Il est vrai que “Air Paradis” n’a pas toujours bonne presse sur les réseaux…

En sortant d’un rendez-vous de cadrage avec les équipes de Air Paradis, vous avez noté les éléments suivants :

    Air Paradis veut un prototype d’un produit IA permettant de prédire le sentiment associé à un tweet.
    Données : pas de données clients chez Air Paradis. Solution : utiliser des données Open Source (ou en téléchargement direct à ce lien)
        Description des données : des informations sur les tweets (utilisateur ayant posté, contenu, moment du post) et un label binaire (tweet exprimant un sentiment négatif ou non). 

    TO-DO :
        Préparer un prototype fonctionnel du modèle. Le modèle envoie un tweet et récupère la prédiction de sentiment. 
        Préparer un support de présentation explicitant la méthodologie utilisée pour l’approche “modèle sur mesure avancé” (attention : audience non technique).

Après avoir reçu votre compte-rendu, Marc, votre manager, vous a contacté pour, selon ses mots, “faire d’une pierre deux coups”.



De : Marc

Envoyé : hier 17:14

À : vous

Objet : Air Paradis : complément

Salut,

Merci pour ton récap du meeting avec Air Paradis. J’ai l’impression que ça s’est bien passé ?!

Je me disais… Puisque tu vas faire un proto pour ce client, j’ai l’intuition que ce produit pourrait se généraliser à d’autres cas d’usage.

Tu voudrais bien en profiter pour tester plusieurs approches ?

    une approche “Modèle sur mesure simple”, pour développer rapidement un modèle classique (ex : régression logistique) permettant de prédire le sentiment associé à un tweet.
    une approche “Modèle sur mesure avancé”, pour développer un modèle basé sur des réseaux de neurones profonds pour prédire le sentiment associé à un tweet. => C’est ce modèle que tu devras déployer et montrer à Air Paradis.



Pour cette 2ème approche, tu penseras bien à essayer au moins deux word embeddings différents et à garder celui qui permet d’obtenir les meilleures performances. En complément, pourrais-tu également regarder l’apport en performance d’un modèle BERT ? Cela nous permettra de voir si nous devons investir dans ce type de modèle.

Et en même ce serait top si tu pouvais mettre en oeuvre un bon exemple de démarche orientée MLOps, tu sais c’est la nouvelle priorité de notre directeur !



J’aimerais que tu puisses démontrer à l’occasion de l’élaboration de ton prototype tout l’apport du MLOps, afin d’assurer une diffusion aux autres équipes :

    d’abord réaliser une présentation synthétique des principes du MLOps et ses apports, 
    ensuite utiliser l’outil MLFlow, future référence pour notre société, pour assurer la gestion des expérimentations des modèles : tracking et reporting de l’entraînement des modèles, centralisation du stockage des modèles, et test du serving proposé par MLFlow, 
    et enfin mettre en œuvre un pipeline de déploiement continu du modèle que tu auras choisi via une API (Git + Github + plateforme Cloud au choix), qui intègre également des tests unitaires automatisés.



Nous souhaitons limiter les coûts de mise en production de ce prototype, donc peux-tu privilégier une solution gratuite Cloud pour l’API ?



Si le modèle avancé est trop lourd et induit un dépassement des limites de taille des solutions gratuites, tu pourras tester le déploiement avec le modèle classique, ou bien utiliser des techniques de réduction de taille de ton modèle TensorFlow-Keras via une conversion en TensorFlow Lite.



Merci d’avance !

Marc

PS : Ah au fait, tant que tu y es, tu pourras rédiger un petit article pour le blog à partir de ton travail de modélisation et de ta démarche orientée MLOps ?

Vous avez pris connaissance du mail, vous avez hâte de démarrer ce nouveau projet avec intérêt ! C’est parti !
Livrables

    Le “Modèle sur mesure avancé”, exposé grâce à un déploiement d’une API sur un service Cloud (par exemple Azure, Heroku avec un compte “Github Student”, Pythonanywhere qui offrent des solutions gratuites, ou toute autre solution), qui recevra en entrée un tweet et retournera le sentiment associé au tweet prédit par le modèle.
    L’ensemble des scripts pour réaliser les trois approches (classique, modèle sur mesure avancé, modèle avancé BERT). 
        Ce livrable intégrera la gestion des expérimentations avec l’outil MLFlow (tracking des expérimentations, enregistrement des modèles) 
    Un article de blog de 1000 à 1200 mots environ contenant : 
        une présentation et une comparaison des trois approches (“Modèle sur mesure simple” et “Modèle sur mesure avancé”, “Modèle avancé BERT”)
        La démarche orientée MLOps mise en oeuvre
    Un support de présentation (type PowerPoint) de votre démarche méthodologique, des résultats des différents modèles élaborés via la mise en oeuvre d’expérimentations MLFlow et de sa visualisation via l’UI (User Interface) de MLFlow,  et de la mise en production d’un modèle avancé.
        Des copies écran des commits, du dossier Github (+ lien vers ce dossier) et de l’exécution des tests unitaires, qui sont les preuves qu’un pipeline de déploiement continu a permis de déployer l’API, doivent être formalisés dans ce support de présentation.

Pour faciliter votre passage devant le jury, déposez sur la plateforme, dans un dossier zip nommé “Titre_du_projet_nom_prénom”, votre livrable nommé comme suit : Nom_Prénom_n° du livrable_nom du livrable_date de démarrage du projet. Cela donnera :

    Nom_Prénom_1_modele_mmaaaa
    Nom_Prénom_2_scripts_mmaaaa
    Nom_Prénom_3_article_de_blog_mmaaaa
    Nom_Prénom_4_presentation_mmaaaa

Par exemple, votre premier livrable peut être nommé comme suit : Dupont_Jean_1_modele_012022.
Soutenance

Pendant la soutenance, l’évaluateur ne jouera aucun rôle en particulier. Vous lui présenterez l’ensemble de votre travail.

    Présentation (20 minutes) 
        Elaboration du modèle selon l’approche “modèle sur mesure simple” (3 minutes)
        Elaboration des modèles de l’approche “modèle sur mesure avancé”, et modèle avancé BERT, et ses simulations (7 minutes).
        Résultats comparés des modèles (5 minutes)
            Synthèse des principes du MLOps (1 ou 2 slides)
            Démarche d’expérimentation des modèles avec MLFlow
            Tableau de synthèse des scores des modèles et visualisation des résultats détaillés via l’UI de MLFlow
        Mise en production d’un modèle sur mesure avancé (5 minutes) :
            Démarche de déploiement continu en production sur le Cloud choisi par l’étudiant(moteur d’inférence via création d’une API)
            Démonstration de fonctionnement d’appel au moteur d’inférence, en testant un exemple de tweet, afin de déterminer un sentiment positif ou négatif.
    Discussion (5 minutes)
        L’évaluateur vous challengera sur vos choix. 
    Débriefing (5 minutes)
        À la fin de la soutenance, vous pourrez débriefer ensemble. 

Votre présentation devrait durer 20 minutes (+/- 5 minutes).  Puisque le respect des durées des présentations est important en milieu professionnel, les présentations en dessous de 15 minutes ou au-dessus de 25 minutes peuvent être refusées.

Concernant la mise en production de l’API, plusieurs solutions s’offrent à vous, en particulier Azure webapp, AWS et Heroku. À vous de choisir la solution qui vous convient le mieux. Voici ci-dessous quelques informations sur chacune de ces solutions. Quelque soit la solution Cloud choisie, l'étudiant et l'évaluateur veilleront à enregistrer pendant la soutenance la démo de l'application en production, ce qui permettra au jury de visionner cette démo, sans que l'étudiant n'ait à maintenir son application sur le Cloud. Maintenir l’application dans le Cloud pourrait en effet engendrer des coûts.

Heroku


Dans le cadre de l’utilisation de Heroku, étant devenu payant depuis fin novembre 2022, les coûts liés à votre projet seront à votre charge. Vous êtes donc libre de vous investir financièrement si vous le souhaitez, mais vous n’avez aucune obligation de le faire pour réaliser ce projet.

Azure webapp


Il est possible d’avoir une utilisation gratuite d’Azure, en restant en dessous des seuils de gratuité (1 Go). Voici une FAQ créée par un mentor OpenClassrooms à ce sujet.

AWS


AWS met à disposition gratuitement des instances EC2 de type T2 micro (1 Go) pendant 12 mois (750 heures par mois) :

    Offre : https://aws.amazon.com/fr/free/ 
    Instances : https://aws.amazon.com/fr/ec2/instance-types/

Au delà des 12 mois, le coût est de 1,3 centimes d’Euro / heure, soit 10 Euros par mois.

Etant donné que le besoin ne concerne que le test de l’API et sa démo en soutenance, donc au maximum quelques heures ou jours, le coût restera très limité, inférieur à 1 Euros.
Référentiel d'évaluation



Compétences


Critères d'évaluation

Entraîner un modèle Deep Learning sur des données textuelles


CE1 Le candidat a défini sa stratégie d’élaboration d’un modèle pour répondre à un besoin métier (par exemple : choix de conception d’un modèle ou ré-utilisation de modèles pré-entraînés).

CE2 Le candidat a identifié la ou les cibles.

CE3 Le candidat a réalisé la séparation du jeu de données en jeu d’entraînement, jeu de validation et jeu de test.

CE4 Le candidat s'est assuré qu'il n’y a pas de fuite d’information entre les deux jeux de données (entraînement, validation et test).

CE5 Le candidat a testé plusieurs modèles d’apprentissage profond en partant du plus simple vers les plus complexes :

    dont au moins un modèle Tensorflow/Keras de base avec embedding, un modèle Tensorflow/Keras avec embedding et couche LSTM, ainsi qu’un modèle BERT.

CE6 Le candidat a mis en oeuvre des modèles à partir de modèles pré-entraînés (technique de Transfer Learning)

Choisir la méthode de plongement de mots pertinente pour un modèle de Deep Learning


CE1 Le candidat, en complément de la démarche de type “bag-of-words”, a mis en oeuvre 3 démarches de word/sentence embedding : Word2Vec (ou Doc2Vec ou Glove ou FastText), BERT, et USE (Universal Sentence Encoder) :

    Au moins deux méthodes d’embedding ont été testées (parmi word2vec, Glove, fasttext) pour les “modèles avancés” Tensorflow/Keras.
    Les données sont préparées (input ids, attention mask) pour mettre en oeuvre une démarche d’embedding pour le “modèle BERT”
    Une démarche optionnelle d’embedding via USE est mise en oeuvre

Sélectionner les méthodes de prétraitement du texte pour un modèle de Deep Learning


CE1 Au moins deux techniques de prétraitement du texte ont été testées (ex : lemmatization, stemming…)

Évaluer la performance d’un modèle de Deep Learning sur des données textuelles


CE1 Le candidat a choisi une métrique adaptée à la problématique métier, et sert à évaluer la performance des modèles

CE2 Le candidat a explicité le choix de la métrique d’évaluation

CE3 Le candidat a évalué la performance d’un modèle de référence et sert de comparaison pour évaluer la performance des modèles plus complexes

CE4 Le candidat a calculé, hormis la métrique choisie, au moins un autre indicateur pour comparer les modèles (par exemple : le temps nécessaire pour l’entraînement du modèle)

CE5 Le candidat a optimisé au moins un des hyperparamètres du modèle choisi (par exemple : le choix de la fonction Loss, le Batch Size, le nombre d'Epochs)

CE6 Le candidat a présenté une synthèse comparative des différents modèles, par exemple sous forme de tableau.

Définir et mettre en œuvre un pipeline d’entraînement des modèles, avec centralisation du stockage des modèles et formalisation des résultats et mesures des différentes expérimentations réalisées, afin d’industrialiser le projet de Machine Learning.




CE1 Le candidat a mis en oeuvre un pipeline d’entraînement des modèles reproductible

CE2 Le candidat a sérialisé et stocké les modèles créés dans un registre centralisé afin de pouvoir facilement les réutiliser.

CE3 Le candidat a formalisé des mesures et résultats de chaque expérimentation, afin de les analyser et de les comparer

Mettre en œuvre un logiciel de version de code afin d’assurer en continu l’intégration et la diffusion du modèle auprès de collaborateurs.


CE1 Le candidat a créé un dossier contenant tous les scripts du projet dans un logiciel de version de code (ex : Git) et l'a partagé (ex : Github).

CE2 Le candidat a présenté un historique des modifications du projet qui affiche au moins trois versions distinctes, auxquelles il est possible d'accéder.

CE3 Le candidat a tenu à jour et mis à disposition la liste des packages utilisés ainsi que leur numéro de version .

CE4 Le candidat a rédigé un fichier introductif permettant de comprendre l'objectif du projet et le découpage des dossiers.

CE5 Le candidat a commenté les scripts et les fonctions facilitant une réutilisation du travail par d'autres personnes et la collaboration.

Concevoir et assurer un déploiement continu d'un moteur d’inférence (modèle de prédiction encapsulé dans une API) sur une plateforme Cloud afin de permettre à des applications de réaliser des prédictions via une requête à l’API.


CE1 Le candidat a défini et préparé un pipeline de déploiement continu.

CE2 Le candidat a déployé le modèle de machine learning sous forme d'API (via Flask par exemple) et cette API renvoie bien une prédiction correspondant à une demande.

CE3 Le candidat a mis en œuvre un pipeline de déploiement continu, afin de déployer l'API sur un serveur d'une plateforme Cloud.

CE4 Le candidat a mis en oeuvre des tests unitaires automatisés (par exemple avec pyTest)

CE5 Le candidat a réalisé l'API indépendamment de l'application qui utilise le résultat de la prédiction

Rédiger une note méthodologique contenant notamment le choix des algorithmes testés, les métriques utilisées et l’interprétabilité du modèle proposé, afin de communiquer sa démarche de modélisation


CE1 Le candidat a présenté la démarche de modélisation de manière synthétique dans une note.

CE2 Le candidat a explicité la métrique d'évaluation retenue et sa démarche d'optimisation.

CE3 Le candidat a explicité l'interprétabilité globale et locale du modèle.

CE4 Le candidat a décrit les limites et les améliorations envisageables pour gagner en performance et en interprétabilité de l'approche de modélisation

Réaliser la présentation orale d’une démarche de modélisation à un client interne/externe afin de partager les résultats et faciliter la prise de décision de l'interlocuteur


CE1 Le candidat a expliqué de manière compréhensible par un public non technique la méthode d'évaluation de la performance du modèle de machine learning, la façon d'interpréter les résultats du modèle, et la façon d'interpréter l'importance des variables du modèle.

CE2 Le candidat a su répondre de manière simple (compréhensible par un public non technique) à au moins une question portant sur sa démarche de modélisation.

CE3 Le candidat a présenté une démarche de modélisation et une évaluation complète des modèles, en particulier la comparaison de plusieurs modèles


Compétences évaluées

Choisir la méthode de plongement de mots pertinente pour un modèle de Deep Learning
Entraîner un modèle Deep Learning sur des données textuelles
Évaluer la performance d’un modèle de Deep Learning sur des données textuelles
Sélectionner les méthodes de prétraitement du texte pour un modèle de Deep Learning
Définir et mettre en œuvre un pipeline d’entraînement des modèles
Mettre en œuvre un logiciel de version de code
Réaliser la présentation orale d’une démarche de modélisation
Rédiger une note méthodologique afin de communiquer sa démarche de modélisation
Concevoir et assurer un déploiement continu d'un moteur d’inférence dans le Cloud